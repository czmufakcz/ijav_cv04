
public class Trader extends User {

    public Trader(int age, String name) {
        super(age, name);
    }

    @Override
    public ROLE getRole() {
        return ROLE.TRADER;
    }

}
