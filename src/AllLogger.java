import java.util.LinkedList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;

public class AllLogger implements Observer {

    private List<LoggerMessage<String, Level>> logger = new LinkedList<>();

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof LoggerMessage<?, ?>) {
            logger.add((LoggerMessage<String, Level>) arg);
        }
    }

    public void printAllMessages() {
        logger.stream()
              .forEach(System.out::println);
    }

}
