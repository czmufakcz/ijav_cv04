
public class UserFactory {
    
    private UserFactory() {
        
    }
    
    public static User createUser(ROLE role, int age, String name) {
        switch (role) {
        case ADMIN:
            return new Admin(age, name);
        case CUSTOMER:
            return new Customer(age, name);
        case TRADER:
            return new Trader(age, name);
        default:
            throw new IllegalArgumentException();
        }
    }
}
