import java.util.Collection;
import java.util.Optional;

public class Main {

    public static void main(String[] args) {
        AllLogger allLogger = new AllLogger();
        InfoLogger infoLogger = new InfoLogger();
        UserRepository userRepository = new UserRepository();

        userRepository.addObserver(allLogger);
        userRepository.addObserver(infoLogger);

        userRepository.addUser(UserFactory.createUser(ROLE.ADMIN, 15, "Josef"));
        userRepository.addUser(UserFactory.createUser(ROLE.ADMIN, 27, "Jan"));
        userRepository.addUser(UserFactory.createUser(ROLE.ADMIN, 27, "Michael"));
        userRepository.addUser(UserFactory.createUser(ROLE.CUSTOMER, 30, "Martin"));
        userRepository.addUser(UserFactory.createUser(ROLE.CUSTOMER, 35, "Zdenek"));
        userRepository.addUser(UserFactory.createUser(ROLE.TRADER, 27, "Karel"));

        Optional<User> zdenekUser = userRepository.findUserByName("Zdenek");

        System.out.println("---------------------");
        System.out.println(zdenekUser);
        System.out.println("---------------------");
        Collection<User> collections = userRepository.getAllUsersGreaterThanAgeAndSortByNameASC(27);
        userRepository.printAllUsers(collections);
        System.out.println("---------INFO LOGGER------------");
        infoLogger.printAllMessages();
        System.out.println("---------ALL LOGGER------------");
        allLogger.printAllMessages();
        System.out.println("---------ANNOTATIONS------------");
        userRepository.getCollection()
                      .stream()
                      .forEach(user -> {
                          Class<?> clazz = user.getClass();
                          if(clazz.isAnnotationPresent(MyAnnotation.class))
                              System.out.println(user);
                      });

    }

}
