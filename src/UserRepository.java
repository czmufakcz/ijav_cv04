import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Observable;
import java.util.Optional;
import java.util.logging.Level;
import java.util.stream.Collectors;

public class UserRepository extends Observable implements IUserRepository {

    private final List<User> users = new LinkedList<>();

    @Override
    public void addUser(User user) {
        if (Optional.ofNullable(user)
                    .isPresent()) {
            users.add(user);
            sendLog("User was succesfully added: " + user, Level.INFO);
        } else
            sendLog("Failed to add user", Level.WARNING);
    }

    private void sendLog(String message, Level level) {
        this.setChanged();
        this.notifyObservers(new LoggerMessage<String, Level>(message, level));
    }

    @Override
    public Optional<User> findUserByName(String name) {
        return users.stream()
                    .filter(user -> user.getName()
                                        .equals(name))
                    .findFirst();
    }

    @Override
    public Collection<User> getAllUsersGreaterThanAgeAndSortByNameASC(int age) {
        return users.stream()
                    .filter(user -> user.getAge() > age)
                    .sorted((user, user1) -> user.getName()
                                                 .compareTo(user1.getName()))
                    .collect(Collectors.toList());

    }

    @Override
    public Collection<User> getCollection() {
        return users;
    }

}
