
public class LoggerMessage<T1, T2> {
    private T1 message;
    private T2 level;

    public LoggerMessage(T1 message, T2 level) {
        this.message = message;
        this.level = level;
    }

    public T1 getMessage() {
        return message;
    }

    public void setMessage(T1 message) {
        this.message = message;
    }

    public T2 getLevel() {
        return level;
    }

    public void setLevel(T2 level) {
        this.level = level;
    }
    
    @Override
    public String toString() {
        return "LoggerMessage [message=" + message + ", level=" + level + "]";
    }

}
