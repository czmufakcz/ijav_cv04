
@MyAnnotation
public class Admin extends User {

    public Admin(int age, String name) {
        super(age, name);
    }

    @Override
    public ROLE getRole() {
        return ROLE.ADMIN;
    }

}
