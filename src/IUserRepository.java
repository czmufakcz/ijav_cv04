import java.util.Collection;
import java.util.Optional;

public interface IUserRepository {

    void addUser(User user);

    Optional<User> findUserByName(String name);

    Collection<User> getAllUsersGreaterThanAgeAndSortByNameASC(int age);
    
    Collection<User> getCollection();

    default void printAllUsers(Collection<User> users) {
        users.stream()
             .forEach(System.out::println);
    }
}
