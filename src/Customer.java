
public class Customer extends User {

    public Customer(int age, String name) {
        super(age, name);
    }

    @Override
    public ROLE getRole() {
        return ROLE.CUSTOMER;
    }

}
