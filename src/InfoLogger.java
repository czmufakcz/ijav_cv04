import java.util.LinkedList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;

public class InfoLogger implements Observer{

    private List<LoggerMessage<String, Level>> logger = new LinkedList<>();

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof LoggerMessage<?, ?>) {
            LoggerMessage<String, Level> message = (LoggerMessage<String, Level>) arg;
            if(message.getLevel() == Level.INFO) {
                logger.add(message);
            }
        }
    }

    public void printAllMessages() {
        logger.stream()
              .forEach(System.out::println);
    }
}
