# Zadání úkolů

1. Zkopírujte třídy User, Admin,Trader, Customer,  UserFactory a enum ROLE z minulého cvičení
2. Vytvořte rozhraní IUserRepository, která má tyto metody:
    + void addUser(User user);
    + Optional<User> findUserByName(String name);
    + Collection<User> getAllUsersGreaterThanAgeAndSortByNameASC(int age);
    + Collection<User> getCollection()
    + default void printAllUsers(Collection<User> users)
3. Implementujte tělo defaultní metodu pro rozhraní IUserRepository
    + Využíte odkazy metod pro System.out.println()
4. Vytvořte generickou třídu LoggerMessage<T1, T2>
    + Bude reprezentovat návrhový vzor Přepravka (Message)
    + T1 pojmenujte jako atribut message
    + T2 pojmenujte jako atribut level
    + Vytvořte konstruktor, který přijímá tyto dva typy
    + Vygenerujte getters and setters
5. Implementujte návrhový vzor Pozorovatel (Observer)
6. Vytvořte třídu AllLogger implements Observer
    + Vytvořte zde kolekci List<LoggerMessage<String, Level>> logger implementací LinkedList
    + Implementujte metodu void update(Observable obs, Object arg)
        + Bude pouze přidávat přijímané zprávy do logger
    + Vytvořte a implementujte metodu printAllMessages()
7. Vytvořte obdobně třídu InfoLogger jako u bodu 6
    + Bude pouze přijímat zprávy typu INFO do logger
8. Vytvořte třídu UserRepository extends Observable implements IUserRepository
    + Vytvořte zde kolekci List<User> users s implementací LinkedList
    + addUser -> ověřte null pomocí Optional, pokud nebude null přidejte do users.
        + V obouch případech pošlete zprávu Observeru přes metody setChanged and notifyObservers.
        + Úspěšné přidání -> Zpráva bude typu INFO a obsah zprávy bude “User successfully is added: ” + user.
        + Neúspěšné přidání -> Zpráva bude typu WARNING a obsah zprávy bude “Failed to add user.”.
    + findUserByName -> implementujte pomocí streamu a jeho příslušných metod. Nalezněte prvního a vraťte.
    + getAllUsersGreaterThanAgeAndSortByNameASC ->  implementujte pomocí streamu a jeho příslušných metod
    + vrátí všechny uživatelé, kteří budou splňovat věk větší jak daný parametr a budou seřazeny pod jména vzestupně
9. V metodě Main:
    + AllLogger allLogger = new AllLogger();
    + InfoLogger infolLogger = new InfoLogger ();
    + UserRepository userRepository = new UserRepository();
    + Přihlašte oba loggery k Observable.
    + Do userRepository vložte přes tovární metodu tyto data:
        + ADMIN - age: 15, name: Josef
        + ADMIN - age: 27, name: Jan
        + ADMIN - age: 27, name: Michael
        + CUSTOMER - age: 30, name: Martin
        + CUSTOMER - age: 35, name: Zdenek
        + TRADER - age: 27, name: Karel
    + findUserByName -> Zadejte parametr “Zdenek” a vypište.
    + getAllUsersGreaterThanAgeAndSortByNameASC -> Zadejte parametr 27 a vypište pomocí metody printAllUsers.
    + Vypište allLogger.
    + Vypište infolLogger.
10. Implementujte vlastní anotaci MyAnnotation.
    + Doplňte anotaci @Target a @Retention
    + Doplňte anotaci nad třídu Admin
    + V metodě main projděte všechny v kolekci userRepository a vypište ty, kteří mají anotaci Admin

